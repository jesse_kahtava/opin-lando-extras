To use these extra commands add the contents of lando-extras.yaml to the the
'tooling' section of your .lando.yml file.

Alternatively, from this diretory run:
php -r 'require "LandoExtras.php"; Pod1\LandoExtras::installYaml();'
