<?php

namespace Pod1;

require('../../autoload.php');

use Composer\Script\Event;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Dumper;

class LandoExtras {
  public static function installYaml($event = NULL) {
    $lando_yml_filename = '../../../.lando.yml';
    $lando_yml_extras_filename = 'lando-extras.yml';

    $lando_yaml = Yaml::parseFile($lando_yml_filename);
    $lando_extras_yaml = Yaml::parseFile($lando_yml_extras_filename);

    $lando_yaml['tooling'] = array_merge($lando_yaml['tooling'], $lando_extras_yaml['tooling']);

    // Initialize Dumper with standard indentation of 2 spaces.
    $yaml_dumper = new Dumper('2');
    // Output yaml. Don't go inline until 10 levels deep.
    $new_yaml = $yaml_dumper->dump($lando_yaml, 10);

    file_put_contents($lando_yml_filename, $new_yaml);
  }

}
