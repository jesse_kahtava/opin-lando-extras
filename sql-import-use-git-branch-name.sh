#!/bin/bash

# Set generic config
FILE=""
WIPE=true
HOST=localhost

GREEN='\033[0;32m'
RED='\033[0;31m'
DEFAULT_COLOR='\033[0;0m'

# Get type-specific config
if [[ ${POSTGRES_DB} != '' ]]; then
  DATABASE=${POSTGRES_DB:-database}
  PORT=5432
  USER=postgres
else
  DATABASE=${MYSQL_DATABASE:-database}
  PORT=3306
  USER=root
fi

GITHEAD=$(cat $LANDO_MOUNT/.git/HEAD)
GITREF=${GITHEAD##ref: refs\/heads\/}
DUMPFILENAME=${GITREF//[\/]/_}.sql;
DUMPSDIR=$LANDO_MOUNT/sql_dumps
FILE="$DUMPSDIR/$DUMPFILENAME.gz"

if [ ! -f $FILE ]; then
  printf "${RED}SQL dump file $FILE does not exist."
  exit 1
fi

read -r -p "Import file $DUMPFILENAME? (Y/N) " RESPONSE;
if [[ ! $RESPONSE =~ [yY] ]]; then
  exit 1
fi

CMD="$FILE"

# Inform the user of things
echo "Preparing to import $FILE into $DATABASE on $HOST:$PORT as $USER..."

# Wipe the database if set
if [ "$WIPE" == "true" ]; then

  echo "Destroying all current tables in $DATABASE... "
  echo "NOTE: See the --no-wipe flag to avoid this step!"


  # DO db specific wiping
  if [[ ${POSTGRES_DB} != '' ]]; then

    # Drop and recreate database
    printf "\t\t${GREEN}Dropping database ...\n\n${DEFAULT_COLOR}"
    psql postgresql://$USER@$HOST:$PORT/postgres -c "drop database $DATABASE"

    printf "\t\t${GREEN}Creating database ...\n\n${DEFAULT_COLOR}"
    psql postgresql://$USER@$HOST:$PORT/postgres -c "create database $DATABASE"

  else

    # Build the SQL prefix
    SQLSTART="mysql -h $HOST -P $PORT -u $USER $DATABASE"

    # Gather and destroy tables
    TABLES=$($SQLSTART -e 'SHOW TABLES' | awk '{ print $1}' | grep -v '^Tables' )

    # PURGE IT ALL! BURN IT TO THE GROUND!!!
    for t in $TABLES; do
      echo "Dropping $t table from $DATABASE database..."
      $SQLSTART -e "DROP TABLE $t"
    done

  fi
fi

# Check to see if we have any unzipping options or GUI needs
if command -v gunzip >/dev/null 2>&1 && gunzip -t $FILE >/dev/null 2>&1; then
  echo "Gzipped file detected!"
  if command -v pv >/dev/null 2>&1; then
    CMD="pv $CMD"
  else
    CMD="cat $CMD"
  fi
  CMD="$CMD | gunzip"
elif command -v unzip >/dev/null 2>&1 && unzip -t $FILE >/dev/null 2>&1; then
  echo "Zipped file detected!"
  CMD="unzip -p $CMD"
  if command -v pv >/dev/null 2>&1; then
    CMD="$CMD | pv"
  fi
else
  if command -v pv >/dev/null 2>&1; then
    CMD="pv $CMD"
  else
    CMD="cat $CMD"
  fi
fi

# Build DB specific import command
if [[ ${POSTGRES_DB} != '' ]]; then
  CMD="$CMD | psql postgresql://$USER@$HOST:$PORT/$DATABASE"
else
  CMD="$CMD | mysql -h $HOST -P $PORT -u $USER $DATABASE"
fi

# Import
echo "Importing $FILE..."
if command eval "$CMD"; then
  STATUS=$?
else
  STATUS=1
fi

# Finish up!
if [ $STATUS -eq 0 ]; then
  echo ""
  printf "${GREEN}Import complete!${DEFAULT_COLOR}"
  echo ""
else
  echo ""
  printf "${RED}Import failed.${DEFAULT_COLOR}"
  exit $STATUS
  echo ""
fi
